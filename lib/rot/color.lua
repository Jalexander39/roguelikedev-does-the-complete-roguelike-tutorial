--- The Color Toolkit.
-- Color is a color handler that treats any
-- objects intended to represent a color as a
-- table of the following schema:
-- @module ROT.Color

local ROT = require((...):gsub(('.[^./\\]*'):rep(1) .. '$', ''))
local Color = ROT.Class:extend("Color")

function Color:init(r, g, b, a)
    self[1], self[2], self[3], self[4] = r or 0, g or 0, b or 0, a or 255
end

--- Get color from string.
-- Convert one of several formats of string to what
-- Color interperets as a color object
-- @tparam string str Accepted formats 'rgb(0..255, 0..255, 0..255)', '#5fe', '#5FE', '#254eff', 'goldenrod'
function Color.fromString(str)
    local cached = Color._cached[str]
    if cached then return cached end
    local values = { 0, 0, 0 }
    if str:sub(1,1) == '#' then
        local i=1
        for s in str:gmatch('[%da-fA-F]') do
            values[i]=tonumber(s, 16)
            i=i+1
        end
        if #values==3 then
            for i=1,3 do values[i]=values[i]*17 end
        else
            for i=1, 3 do
                values[i+1]=values[i+1]+(16*values[i])
                table.remove(values, i)
            end
        end
    elseif str:gmatch('rgb') then
        local i=1
        for s in str:gmatch('(%d+)') do
            values[i]=tonumber(s)
            i=i+1
        end
    end
    Color._cached[str] = values
    return values
end

local function add(t, color, ...)
    if not color then return t end
    for i = 1, #color do
        t[i] = (t[i] or 0) + color[i]
    end
    return add(t, ...)
end

local function multiply(t, color, ...)
    if not color then return t end
    for i = 1, #color do
        t[i] = math.floor((t[i] or 255) * color[i] / 255 + 0.5)
    end
    return multiply(t, ...)
end

--- Add two or more colors.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam table ... More color tables
-- @treturn table new color
function Color.add(...) return add({}, ...) end

--- Add two or more colors. Modifies first color in-place.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam table ... More color tables
-- @treturn table modified color
function Color.add_(...) return add(...) end

-- Multiply (mix) two or more colors.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam table ... More color tables
-- @treturn table new color
function Color.multiply(...) return multiply({}, ...) end

-- Multiply (mix) two or more colors. Modifies first color in-place.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam table ... More color tables
-- @treturn table modified color
function Color.multiply_(...) return multiply(...) end

--- Interpolate (blend) two colors with a given factor.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam float factor A number from 0 to 1. <0.5 favors color1, >0.5 favors color2.
-- @treturn table resulting color
function Color.interpolate(color1, color2, factor)
    factor = factor or .5
    local result = {}
    for i = 1, math.max(#color1, #color2) do
        local a, b = color2[i] or color1[i], color1[i] or color2[i]
        result[i] = math.floor(b + factor*(a-b) + 0.5)
    end
    return result
end

--- Interpolate (blend) two colors with a given factor in HSL mode.
-- @tparam table color1 A color table
-- @tparam table color2 A color table
-- @tparam float factor A number from 0 to 1. <0.5 favors color1, >0.5 favors color2.
-- @treturn table resulting color
function Color.interpolateHSL(color1, color2, factor)
    factor = factor or .5
    local result = {}
    local hsl1, hsl2 = Color.rgb2hsl(color1), Color.rgb2hsl(color2)
    for i = 1, math.max(#hsl1, #hsl2) do
        local a, b = hsl2[i] or hsl1[i], hsl1[i] or hsl2[i]
        result[i] = b + factor*(a-b)
    end
    return Color.hsl2rgb(result)
end

--- Create a new random color based on this one
-- @tparam table color A color table
-- @tparam int|table diff One or more numbers to use for a standard deviation
function Color.randomize(color, diff, rng)
    rng = rng or color._rng or ROT.RNG
    local result = {}
    if type(diff) ~= 'table' then
        local diff = rng:random(0, diff)
        for i = 1, #color do
            result[i] = color[i] + diff
        end
    else
        for i = 1, #color do
            result[i] = color[i] + rng:random(0, diff[i])
        end
    end
    return result
end

-- Convert rgb color to hsl
function Color.rgb2hsl(color)
    local r=color[1]/255
    local g=color[2]/255
    local b=color[3]/255
    local a=color[4] and color[4]/255
    local max=math.max(r, g, b)
    local min=math.min(r, g, b)
    local h,s,l=0,0,(max+min)/2

    if max~=min then
        local d=max-min
        s=l>.5 and d/(2-max-min) or d/(max+min)
        if max==r then
            h=(g-b)/d + (g<b and 6 or 0)
        elseif max==g then
            h=(b-r)/d + 2
        elseif max==b then
            h=(r-g)/ d + 4
        end
        h=h/6
    end

    return { h, s, l, a }
end

local function hue2rgb(p, q, t)
    if t<0 then t=t+1 end
    if t>1 then t=t-1 end
    if t<1/6 then return (p+(q-p)*6*t) end
    if t<1/2 then return q end
    if t<2/3 then return (p+(q-p)*(2/3-t)*6) end
    return p
end

-- Convert hsl color to rgb
function Color.hsl2rgb(color)
    local h, s, l = color[1], color[2], color[3]
    local result = {}
    result[4] = color[4] and math.floor(color[4] * 255)
    if s == 0 then
        local value = math.floor(l * 255 + 0.5)
        for i = 1, 3 do
            result[i] = value
        end
    else
        local q=l<.5 and l*(1+s) or l+s-l*s
        local p=2*l-q
        result[1] = math.floor(hue2rgb(p,q,h+1/3)*255 + 0.5)
        result[2] = math.floor(hue2rgb(p,q,h)*255 + 0.5)
        result[3] = math.floor(hue2rgb(p,q,h-1/3)*255 + 0.5)
    end
    return result
end

--- Convert color to RGB string.
-- Get a string that can be fed to Color.fromString()
-- @tparam table color A color table
function Color.toRGB(color)
    return ('rgb(%d,%d,%d)'):format(
        Color._clamp(color[1]), Color._clamp(color[2]), Color._clamp(color[3]))
end

--- Convert color to Hex string
-- Get a string that can be fed to Color.fromString()
-- @tparam table color A color table
function Color.toHex(color)
    return ('#%02x%02x%02x'):format(
        Color._clamp(color[1]), Color._clamp(color[2]), Color._clamp(color[3]))
end

-- limit a number to 0..255
function Color._clamp(n)
    return n<0 and 0 or n>255 and 255 or n
end

function Color.__add(a, b) return add({}, a, b) end
function Color.__mul(a, b) return mul({}, a, b) end

--- Color cache
-- A table of predefined color tables
-- These keys can be passed to Color.fromString()
-- @field black { 0, 0, 0 }
-- @field navy { 0, 0, 128 }
-- @field darkblue { 0, 0, 139 }
-- @field mediumblue { 0, 0, 205 }
-- @field blue { 0, 0, 255 }
-- @field darkgreen { 0, 100, 0 }
-- @field green { 0, 128, 0 }
-- @field teal { 0, 128, 128 }
-- @field darkcyan { 0, 139, 139 }
-- @field deepskyblue { 0, 191, 255 }
-- @field darkturquoise { 0, 206, 209 }
-- @field mediumspringgreen { 0, 250, 154 }
-- @field lime { 0, 255, 0 }
-- @field springgreen { 0, 255, 127 }
-- @field aqua { 0, 255, 255 }
-- @field cyan { 0, 255, 255 }
-- @field midnightblue { 25, 25, 112 }
-- @field dodgerblue { 30, 144, 255 }
-- @field forestgreen { 34, 139, 34 }
-- @field seagreen { 46, 139, 87 }
-- @field darkslategray { 47, 79, 79 }
-- @field darkslategrey { 47, 79, 79 }
-- @field limegreen { 50, 205, 50 }
-- @field mediumseagreen { 60, 179, 113 }
-- @field turquoise { 64, 224, 208 }
-- @field royalblue { 65, 105, 225 }
-- @field steelblue { 70, 130, 180 }
-- @field darkslateblue { 72, 61, 139 }
-- @field mediumturquoise { 72, 209, 204 }
-- @field indigo { 75, 0, 130 }
-- @field darkolivegreen { 85, 107, 47 }
-- @field cadetblue { 95, 158, 160 }
-- @field cornflowerblue { 100, 149, 237 }
-- @field mediumaquamarine { 102, 205, 170 }
-- @field dimgray { 105, 105, 105 }
-- @field dimgrey { 105, 105, 105 }
-- @field slateblue { 106, 90, 205 }
-- @field olivedrab { 107, 142, 35 }
-- @field slategray { 112, 128, 144 }
-- @field slategrey { 112, 128, 144 }
-- @field lightslategray { 119, 136, 153 }
-- @field lightslategrey { 119, 136, 153 }
-- @field mediumslateblue { 123, 104, 238 }
-- @field lawngreen { 124, 252, 0 }
-- @field chartreuse { 127, 255, 0 }
-- @field aquamarine { 127, 255, 212 }
-- @field maroon { 128, 0, 0 }
-- @field purple { 128, 0, 128 }
-- @field olive { 128, 128, 0 }
-- @field gray { 128, 128, 128 }
-- @field grey { 128, 128, 128 }
-- @field skyblue { 135, 206, 235 }
-- @field lightskyblue { 135, 206, 250 }
-- @field blueviolet { 138, 43, 226 }
-- @field darkred { 139, 0, 0 }
-- @field darkmagenta { 139, 0, 139 }
-- @field saddlebrown { 139, 69, 19 }
-- @field darkseagreen { 143, 188, 143 }
-- @field lightgreen { 144, 238, 144 }
-- @field mediumpurple { 147, 112, 216 }
-- @field darkviolet { 148, 0, 211 }
-- @field palegreen { 152, 251, 152 }
-- @field darkorchid { 153, 50, 204 }
-- @field yellowgreen { 154, 205, 50 }
-- @field sienna { 160, 82, 45 }
-- @field brown { 165, 42, 42 }
-- @field darkgray { 169, 169, 169 }
-- @field darkgrey { 169, 169, 169 }
-- @field lightblue { 173, 216, 230 }
-- @field greenyellow { 173, 255, 47 }
-- @field paleturquoise { 175, 238, 238 }
-- @field lightsteelblue { 176, 196, 222 }
-- @field powderblue { 176, 224, 230 }
-- @field firebrick { 178, 34, 34 }
-- @field darkgoldenrod { 184, 134, 11 }
-- @field mediumorchid { 186, 85, 211 }
-- @field rosybrown { 188, 143, 143 }
-- @field darkkhaki { 189, 183, 107 }
-- @field silver { 192, 192, 192 }
-- @field mediumvioletred { 199, 21, 133 }
-- @field indianred { 205, 92, 92 }
-- @field peru { 205, 133, 63 }
-- @field chocolate { 210, 105, 30 }
-- @field tan { 210, 180, 140 }
-- @field lightgray { 211, 211, 211 }
-- @field lightgrey { 211, 211, 211 }
-- @field palevioletred { 216, 112, 147 }
-- @field thistle { 216, 191, 216 }
-- @field orchid { 218, 112, 214 }
-- @field goldenrod { 218, 165, 32 }
-- @field crimson { 220, 20, 60 }
-- @field gainsboro { 220, 220, 220 }
-- @field plum { 221, 160, 221 }
-- @field burlywood { 222, 184, 135 }
-- @field lightcyan { 224, 255, 255 }
-- @field lavender { 230, 230, 250 }
-- @field darksalmon { 233, 150, 122 }
-- @field violet { 238, 130, 238 }
-- @field palegoldenrod { 238, 232, 170 }
-- @field lightcoral { 240, 128, 128 }
-- @field khaki { 240, 230, 140 }
-- @field aliceblue { 240, 248, 255 }
-- @field honeydew { 240, 255, 240 }
-- @field azure { 240, 255, 255 }
-- @field sandybrown { 244, 164, 96 }
-- @field wheat { 245, 222, 179 }
-- @field beige { 245, 245, 220 }
-- @field whitesmoke { 245, 245, 245 }
-- @field mintcream { 245, 255, 250 }
-- @field ghostwhite { 248, 248, 255 }
-- @field salmon { 250, 128, 114 }
-- @field antiquewhite { 250, 235, 215 }
-- @field linen { 250, 240, 230 }
-- @field lightgoldenrodyellow { 250, 250, 210 }
-- @field oldlace { 253, 245, 230 }
-- @field red { 255, 0, 0 }
-- @field fuchsia { 255, 0, 255 }
-- @field magenta { 255, 0, 255 }
-- @field deeppink { 255, 20, 147 }
-- @field orangered { 255, 69, 0 }
-- @field tomato { 255, 99, 71 }
-- @field hotpink { 255, 105, 180 }
-- @field coral { 255, 127, 80 }
-- @field darkorange { 255, 140, 0 }
-- @field lightsalmon { 255, 160, 122 }
-- @field orange { 255, 165, 0 }
-- @field lightpink { 255, 182, 193 }
-- @field pink { 255, 192, 203 }
-- @field gold { 255, 215, 0 }
-- @field peachpuff { 255, 218, 185 }
-- @field navajowhite { 255, 222, 173 }
-- @field moccasin { 255, 228, 181 }
-- @field bisque { 255, 228, 196 }
-- @field mistyrose { 255, 228, 225 }
-- @field blanchedalmond { 255, 235, 205 }
-- @field papayawhip { 255, 239, 213 }
-- @field lavenderblush { 255, 240, 245 }
-- @field seashell { 255, 245, 238 }
-- @field cornsilk { 255, 248, 220 }
-- @field lemonchiffon { 255, 250, 205 }
-- @field floralwhite { 255, 250, 240 }
-- @field snow { 255, 250, 250 }
-- @field yellow { 255, 255, 0 }
-- @field lightyellow { 255, 255, 224 }
-- @field ivory { 255, 255, 240 }
-- @field white { 255, 255, 255 }
-- @table Color._cache

Color._cached={
    black=                  { 0.00, 0.00, 0.00 },
    navy=                   { 0.00, 0.00, 0.50 },
    darkblue=               { 0.00, 0.00, 0.55 },
    mediumblue=             { 0.00, 0.00, 0.80 },
    blue=                   { 0.00, 0.00, 1.00 },
    darkgreen=              { 0.00, 0.39, 0.00 },
    green=                  { 0.00, 0.50, 0.00 },
    teal=                   { 0.00, 0.50, 0.50 },
    darkcyan=               { 0.00, 0.55, 0.55 },
    deepskyblue=            { 0.00, 0.75, 1.00 },
    darkturquoise=          { 0.00, 0.81, 0.82 },
    mediumspringgreen=      { 0.00, 0.98, 0.60 },
    lime=                   { 0.00, 1.00, 0.00 },
    springgreen=            { 0.00, 1.00, 0.50 },
    aqua=                   { 0.00, 1.00, 1.00 },
    cyan=                   { 0.00, 1.00, 1.00 },
    midnightblue=           { 0.10, 0.10, 0.44 },
    dodgerblue=             { 0.118, 144, 1.00 },
    forestgreen=            { 0.13, 0.55, 0.13 },
    seagreen=               { 0.18, 0.55, 0.34 },
    darkslategray=          { 0.18, 0.31, 0.31 },
    darkslategrey=          { 0.18, 0.31, 0.31 },
    limegreen=              { 0.20, 0.80, 0.20 },
    mediumseagreen=         { 0.24, 0.70, 0.44 },
    turquoise=              { 0.25, 0.88, 0.82 },
    royalblue=              { 0.25, 0.41, 0.88 },
    steelblue=              { 0.27, 0.51, 0.71 },
    darkslateblue=          { 0.28, 0.24, 0.55 },
    mediumturquoise=        { 0.28, 0.82, 0.80 },
    indigo=                 { 0.29, 0.00, 0.51 },
    darkolivegreen=         { 0.33, 0.42, 0.18 },
    cadetblue=              { 0.37, 0.62, 0.63 },
    cornflowerblue=         { 0.39, 0.58, 0.93 },
    mediumaquamarine=       { 0.40, 0.80, 0.67 },
    dimgray=                { 0.41, 0.41, 0.41 },
    dimgrey=                { 0.41, 0.41, 0.41 },
    slateblue=              { 0.42, 0.35, 0.80 },
    olivedrab=              { 0.42, 0.56, 0.14 },
    slategray=              { 0.44, 0.50, 0.56 },
    slategrey=              { 0.44, 0.50, 0.56 },
    lightslategray=         { 0.47, 0.53, 0.60 },
    lightslategrey=         { 0.47, 0.53, 0.60 },
    mediumslateblue=        { 0.24, 0.41, 0.93 },
    lawngreen=              { 0.49, 0.99, 0.00 },
    chartreuse=             { 0.50, 1.00, 0.00 },
    aquamarine=             { 0.50, 1.00, 0.83 },
    maroon=                 { 0.50, 0.00, 0.00 },
    purple=                 { 0.50, 0.00, 0.50 },
    olive=                  { 0.50, 0.50, 0.00 },
    gray=                   { 0.50, 0.50, 0.50 },
    grey=                   { 0.50, 0.50, 0.50 },
    skyblue=                { 0.53, 0.81, 0.92 },
    lightskyblue=           { 0.53, 0.81, 0.98 },
    blueviolet=             { 0.54, 0.17, 0.89 },
    darkred=                { 0.55, 0.00, 0.00 },
    darkmagenta=            { 0.55, 0.00, 0.55 },
    saddlebrown=            { 0.55, 0.27, 0.07 },
    darkseagreen=           { 0.56, 0.74, 0.56 },
    lightgreen=             { 0.56, 0.93, 0.56 },
    mediumpurple=           { 0.58, 0.44, 0.85 },
    darkviolet=             { 0.58, 0.00, 0.83 },
    palegreen=              { 0.60, 0.98, 0.60 },
    darkorchid=             { 0.60, 0.20, 0.80 },
    yellowgreen=            { 0.60, 0.80, 0.20 },
    sienna=                 { 0.63, 0.32, 0.18 },
    brown=                  { 0.65, 0.16, 0.16 },
    darkgray=               { 0.66, 0.66, 0.66 },
    darkgrey=               { 0.66, 0.66, 0.66 },
    lightblue=              { 0.68, 0.85, 0.90 },
    greenyellow=            { 0.68, 1.00, 0.18 },
    paleturquoise=          { 0.69, 0.93, 0.93 },
    lightsteelblue=         { 0.69, 0.77, 0.87 },
    powderblue=             { 0.69, 0.88, 0.90 },
    firebrick=              { 0.70, 0.13, 0.13 },
    darkgoldenrod=          { 0.72, 0.53, 0.04 },
    mediumorchid=           { 0.73, 0.33, 0.83 },
    rosybrown=              { 0.74, 0.56, 0.56 },
    darkkhaki=              { 0.74, 0.72, 0.42 },
    silver=                 { 0.75, 0.75, 0.75 },
    mediumvioletred=        { 0.78, 0.08, 0.52 },
    indianred=              { 0.80, 0.36, 0.36 },
    peru=                   { 0.80, 0.52, 0.25 },
    chocolate=              { 0.82, 0.41, 0.12 },
    tan=                    { 0.82, 0.71, 0.55 },
    lightgray=              { 0.83, 0.83, 0.83 },
    lightgrey=              { 0.83, 0.83, 0.83 },
    palevioletred=          { 0.85, 0.44, 0.58 },
    thistle=                { 0.85, 0.75, 0.85 },
    orchid=                 { 0.85, 0.44, 0.84 },
    goldenrod=              { 0.85, 0.65, 0.13 },
    crimson=                { 0.86, 0.08, 0.24 },
    gainsboro=              { 0.86, 0.86, 0.86 },
    plum=                   { 0.87, 0.63, 0.87 },
    burlywood=              { 0.87, 0.72, 0.53 },
    lightcyan=              { 0.88, 1.00, 1.00 },
    lavender=               { 0.90, 0.90, 0.98 },
    darksalmon=             { 0.91, 0.59, 0.48 },
    violet=                 { 0.93, 0.51, 0.93 },
    palegoldenrod=          { 0.93, 0.91, 0.67 },
    lightcoral=             { 0.94, 0.50, 0.50 },
    khaki=                  { 0.94, 0.90, 0.55 },
    aliceblue=              { 0.94, 0.97, 1.00 },
    honeydew=               { 0.94, 1.00, 0.94 },
    azure=                  { 0.94, 1.00, 1.00 },
    sandybrown=             { 0.96, 0.64, 0.38 },
    wheat=                  { 0.96, 0.87, 0.70 },
    beige=                  { 0.96, 0.96, 0.86 },
    whitesmoke=             { 0.96, 0.96, 0.96 },
    mintcream=              { 0.96, 1.00, 0.98 },
    ghostwhite=             { 0.97, 0.97, 1.00 },
    salmon=                 { 0.98, 0.50, 0.45 },
    antiquewhite=           { 0.98, 0.92, 0.84 },
    linen=                  { 0.98, 0.94, 0.90 },
    lightgoldenrodyellow=   { 0.98, 0.98, 0.82 },
    oldlace=                { 0.99, 0.96, 0.90 },
    red=                    { 1.00, 0.00, 0.00 },
    fuchsia=                { 1.00, 0.00, 1.00 },
    magenta=                { 1.00, 0.00, 1.00 },
    deeppink=               { 1.00, 0.08, 0.58 },
    orangered=              { 1.00, 0.27, 0.00 },
    tomato=                 { 1.00, 0.39, 0.28 },
    hotpink=                { 1.00, 0.41, 0.71 },
    coral=                  { 1.00, 0.50, 0.31 },
    darkorange=             { 1.00, 0.55, 0.00 },
    lightsalmon=            { 1.00, 0.63, 0.48 },
    orange=                 { 1.00, 0.65, 0.00 },
    lightpink=              { 1.00, 0.71, 0.76 },
    pink=                   { 1.00, 0.75, 0.80 },
    gold=                   { 1.00, 0.84, 0.00 },
    peachpuff=              { 1.00, 0.85, 0.73 },
    navajowhite=            { 1.00, 0.87, 0.68 },
    moccasin=               { 1.00, 0.89, 0.71 },
    bisque=                 { 1.00, 0.89, 0.77 },
    mistyrose=              { 1.00, 0.89, 0.88 },
    blanchedalmond=         { 1.00, 0.92, 0.80 },
    papayawhip=             { 1.00, 0.94, 0.84 },
    lavenderblush=          { 1.00, 0.94, 0.96 },
    seashell=               { 1.00, 0.96, 0.93 },
    cornsilk=               { 1.00, 0.97, 0.86 },
    lemonchiffon=           { 1.00, 0.98, 0.80 },
    floralwhite=            { 1.00, 0.98, 0.94 },
    snow=                   { 1.00, 0.98, 0.98 },
    yellow=                 { 1.00, 1.00, 0.00 },
    lightyellow=            { 1.00, 1.00, 0.88 },
    ivory=                  { 1.00, 1.00, 0.94 },
    white=                  { 1.00, 1.00, 1.00 }
}

return Color

