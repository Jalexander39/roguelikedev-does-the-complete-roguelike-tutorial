local keyInput = {

	actions = {
		moveUp = function(player, levelMap) 
			return player:move( 0, -1, levelMap) 
		end,
		moveUpLeft = function(player, levelMap) 
			return player:move(-1, -1, levelMap)
		end,
		moveUpRight = function(player, levelMap) 
			return player:move( 1, -1, levelMap)
		end,
		moveDown = function(player, levelMap) 
			return player:move( 0,  1, levelMap)
		end,
		moveDownLeft = function(player, levelMap) 
			return player:move(-1,  1, levelMap)
		end,
		moveDownRight = function(player, levelMap) 
			return player:move( 1,  1, levelMap)
		end,
		moveLeft = function(player, levelMap) 
			return player:move(-1,  0, levelMap)
		end,
		moveRight = function(player, levelMap) 
			return player:move( 1,  0, levelMap)
		end,
		-- TODO: Resting should be a skip turn function, not move
		rest = function(player, levelMap) 
			local results = { messages = {}, deadActors = {} }
			return results
		end,

		exit = love.event.quit,
	},
	
	keys = {
		-- Movement, arrow keys
		up = "moveUp",
		down = "moveDown",
		left = "moveLeft",
		right = "moveRight",
		-- Movement, number pad
		kp1 = "moveDownLeft",
		kp2 = "moveDown",
		kp3 = "moveDownRight",
		kp4 = "moveLeft",
		kp5 = "rest",
		kp6 = "moveRight",
		kp7 = "moveUpLeft",
		kp8 = "moveUp",
		kp9 = "moveUpRight",
		-- ESC: exit game
		escape = "exit",
	},
	
}

return keyInput