local class = require 'lib.middleclass'
local ROT = require 'lib.rot'

local AI = {
	walkToActor = function(actor, target, levelMap) 
		local dx = target.x - actor.x
		local dy = target.y - actor.y
		local distance = math.sqrt((dx*dx) + (dy*dy))
		dx = math.floor(dx/distance + 0.5)
		dy = math.floor(dy/distance + 0.5)
		if dx > 1 or dx < -1 then print('DX: ' .. dx) end
		if dy > 1 or dy < -1 then print('DY: ' .. dy) end
		return actor:move(dx, dy, levelMap)
	end,

	wander = function(actor, levelMap) 
		local dx = math.random(3) - 2
		local dy = math.random(3) - 2
		return actor:move(dx, dy, levelMap)
	end,
}

AI.aiBasic = class('aiBasic')
-- Component class: pass object to component on initialization
function AI.aiBasic:initialize(parent) 
	self.parent = parent
end
function AI.aiBasic:move(player, levelMap) 
	if levelMap:getFOV(self.parent.x, self.parent.y) and player then
		return AI.walkToActor(self.parent, player, levelMap)
		
	else
		return AI.wander(self.parent, levelMap)
	end
end


return AI