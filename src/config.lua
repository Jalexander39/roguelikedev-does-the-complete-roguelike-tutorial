local con = {
	SCREEN_WIDTH  = 80,
	SCREEN_HEIGHT = 50,
	TILE_SIZE = 16,

	HUD_WIDTH = 20,
	LOG_HEIGHT = 8,

	MAP_WIDTH  = 60,
	MAP_HEIGHT = 60, 
	-- Room parameters for tuto algorithm
	MAX_ROOM_SIZE = 10,
	MIN_ROOM_SIZE = 5,
	MAX_ROOMS = 60,
	-- Camera width/height must be even
	CAM_WIDTH  = 60, 
	CAM_HEIGHT = 42,

	FOV_RADIUS = 7,

	TILE_FONT = 'font/PxPlus_IBM_CGA.ttf',
}

return con