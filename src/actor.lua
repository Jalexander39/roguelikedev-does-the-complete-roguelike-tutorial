local class = require "lib.middleclass"

local Item = require "src.item"

local Actor = class('Actor')

function Actor:initialize(name, x, y, char, color, hp, sp, str, int, agi, con)
	self.name = name or '???'
	self.x = x or 1
	self.y = y or 1
	self.char = char or '@'
	self.color = color or { 1, 0, 1 }
	-- Combat stats: HP, MP, STR, INT, AGI, CON
	self.maxHP = hp or 50
	self.hp = self.maxHP
	self.maxSP = sp or 20
	self.sp = self.maxSP
	self.str = str or 10 -- STR affects attack damage
	self.int = int or 10 -- INT affects SP, magic, and certain status effects
	self.agi = agi or 10 -- AGI affects speed, evasion, and critical hits
	self.speed = self.agi + 20
	self.con = con or 10 -- CON affects HP, defense, and certain status effects
	-- Inventory, spell list, field of view?
end

function Actor:addHP(amount)
	-- Use this for both healing (+) and damage (-) 
	-- Returns false if actor HP reaches 0
	self.hp = math.min(self.hp + amount, self.maxHP)
	self.hp = math.floor(self.hp + 0.5)
	if self.hp <= 0 then
		self.hp = 0
		return false
		-- TODO: Figure out how to pass actor outside (i.e. remove from scheduler)
	end
	return true
end

function Actor:getSpeed() 
	return self.speed
end

function Actor:attack(target) 
	local results = { messages = {}, deadActors = {} }
	-- Temoprary damage formula until we properly implement stats
	damage = math.floor((self.str+1)/2) - math.floor((target.con+2)/4)
	if damage > 0 then
		table.insert(results.messages, 
			{ (self.name .. ' attacks ' .. target.name.. ', dealing ' ..damage.. ' damage.') })
		if not target:addHP(0-damage) then 
			table.insert(results.messages, {(target.name .. ' is defeated!'), { 1, 0.6, 0.2 }})
			table.insert(results.deadActors, target)
		end	
	else
		table.insert(results.messages,
			{(self.name .. ' attacks '.. target.name .. ", but deals no damage."), { 0.4, 0.4, 0.4 }})
	end
	return results
end

function Actor:move(dx, dy, levelMap)
	-- TODO: Return success, failure, or substitute action
	local results = {}
	local tx = self.x + dx
	local ty = self.y + dy
	-- Is target cell within map boundries?
	if tx < 1 or tx > levelMap.width or ty < 1 or ty > levelMap.height then
		return results
	end
	local tile = levelMap.tiles[tx][ty]
	-- Is target cell blocking?
	if tile.blocks then
		-- Is tile otherwise interactive (i.e. a door)?
		-- Return substitute action if yes
		return results
		-- Other tile interactions (i.e. swimming)?
	end	
	local al = levelMap.actorList
	-- Is another actor at target cell?
	for a in pairs(al) do
		if al[a].x == tx and al[a].y == ty then
			local target = al[a]
			-- Make sure target actor is not self (or an ally)
			if target == self then return results end
			-- Return substitute action (attack, talk)
			results = self:attack(target)
			return results
		end
	end
	-- Finally, move actor if not cancelled or substituted action
	self.x = tx
	self.y = ty
	results.moved = true
	return results
end

return Actor