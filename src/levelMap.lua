local class = require "lib.middleclass"
local ROT = require "lib.rot"

local Actor = require 'src.actor'
local AI = require 'src.ai'
local con = require 'src.config'
local Item = require 'src.item'
local Tile = require "src.tile"



-- So far, Rect is only used by this file
local Rect = class("Rect")
function Rect:initialize(x, y, w, h) 
	self.x1 = x 
	self.x2 = x + w 
	self.y1 = y 
	self.y2 = y + h
end
function Rect:center() 
	-- That +1 ensures rounding to nearest integer instead of always down
	local cx = math.floor((self.x1 + self.x2 + 1) / 2)
	local cy = math.floor((self.y1 + self.y2 + 1) / 2)
	return cx, cy
end
function Rect:intersect(other) 
	return (self.x1 <= other.x2 and self.x2 >= other.x1 and 
			self.y1 <= other.y2 and self.y2 >= other.y1)
end
function Rect:randomPos()
	-- Add +1 due to walls on left/top edge of room
	local rx = math.random(self.x1+1, self.x2)
	local ry = math.random(self.y1+1, self.y2)
	return rx, ry
end



local LevelMap = class("LevelMap")

function LevelMap:initialize(w, h)
	self.width = w or con.MAP_WIDTH
	self.height = h or con.MAP_HEIGHT
	-- Initialize tile array to wall tiles
	self.tiles = {}
	for i=1, self.width do 
		self.tiles[i] = {}
		for j=1, self.height do
			self.tiles[i][j] = Tile:new('#', { 0.6, 0.6, 0.6 }, { 0.2, 0.2, 0.2 }, true, true)
		end
	end
	self.scheduler = ROT.Scheduler.Speed:new()
	self.actorList = {}
	self.itemList = {}
	self.roomList = {}

end

function LevelMap:createRoom(room)
	-- Start carving one tile offset from top and left sides
	for i=room.x1+1, room.x2 do
		for j=room.y1+1, room.y2 do
			self.tiles[i][j] = Tile:new('.', { 0.4, 0.4, 0.4 }, { 0, 0, 0 }, false, false)
		end
	end
end
-- TODO: add small chance for 2-tile wide tunnels
function LevelMap:createTunnelH(x1, x2, y)
	for i=math.min(x1, x2), math.max(x1, x2) do
		self.tiles[i][y] = Tile:new('.', { 0.4, 0.4, 0.4 }, { 0, 0, 0 }, false, false)
	end
end

function LevelMap:createTunnelV(y1, y2, x)
	for j=math.min(y1, y2), math.max(y1, y2) do
		self.tiles[x][j] = Tile:new('.', { 0.4, 0.4, 0.4 }, { 0, 0, 0 }, false, false)
	end
end

function LevelMap:addActor(actor) 
	table.insert(self.actorList, actor)
end

function LevelMap:addItem(item)
	table.insert(self.itemList, item)
end

function LevelMap:removeActor(actor)
	self.scheduler:remove(actor)
	for a in pairs(self.actorList) do
		if self.actorList[a] == actor then
			table.remove(self.actorList, a)
		end
	end
end

function LevelMap:removeItem(item)
	for a in pairs(self.itemList) do
		if self.itemList[a] == actor then
			table.remove(self.itemList, a)
		end
	end
end

function LevelMap:placeActor(actor, x, y)
	actor.x = x or math.random(self.width)
	actor.y = y or math.random(self.height)
end

function LevelMap:killActor(actor)
	local corpse = Item:new(actor.name .. ' corpse', actor.x, actor.y, '%', {0.8, 0, 0})
	self:removeActor(actor)
	self:addItem(corpse)
	return corpse
end

function LevelMap:getFOV(x, y)
	return self.tiles[x][y].inFOV
end

function LevelMap:placeActorsInRooms(roomList, actorTable, chance)
	if not actorTable then
		print("No actor table provided!")
		return
	end
	chance = chance or 0.5
	for r in pairs(self.roomList) do
		-- Skip first room, we don't want monsters at player's start position
		if r > 1 then
			local rx, ry = self.roomList[r]:randomPos()
			if math.random() < chance then
				local a = actorTable[math.random(#actorTable)]
				local actor = Actor:new(a.name, rx, ry, a.char, a.color, a.hp, a.sp,
										a.str, a.int, a.agi, a.con)
				actor.ai = AI.aiBasic:new(actor)
				self:addActor(actor)
			end
		end
	end
end

function LevelMap:generateMaze(player, cellWidth, cellHeight, rand, loop, actorTable, chance) 
	local cellCenter = function(cell)
		local cx = cellWidth/2 + (cell.x-1)*cellWidth
		local cy = cellHeight/2 + (cell.y-1)*cellHeight
		return cx, cy
	end
	-- Generate a maze using a grid of cells, each 4x4 tiles
	cellWidth = cellWidth or 4
	cellHeight = cellHeight or cellWidth
	-- Optional: Add wraparound parameter?
	-- randFactor is the chance of selecting cells from visited list randomly
	-- Set higher for many short stubs, or lower for long winding paths
	rand = rand or 0.2
	-- Loop percentage
	loop = loop or 0.2
	-- Create grid array
	local gridWidth = self.width / cellWidth
	local gridHeight = self.height / cellHeight
	local grid = {}
	for i = 1, gridWidth do
		grid[i] = {}
		for j = 1, gridHeight do
			grid[i][j] = { x = i, y = j, vis = 0 }
		end
	end
	local visited = {}
	-- Select random start cell and add to visited list
	local cx = math.random(gridWidth)
	local cy = math.random(gridHeight)
	-- Carve room in first cell, place player
	local newRoom = Rect:new((cx-1)*cellWidth+1, (cy-1)*cellHeight+1, cellWidth-3, cellHeight-3)
	self:createRoom(newRoom)
	table.insert(self.roomList, newRoom)
	local tx, ty = newRoom:center()
	self:placeActor(player, tx, ty)
	-- Mark cell as visited
	grid[cx][cy].vis = grid[cx][cy].vis +  1
	table.insert(visited, grid[cx][cy])
	-- Select cell from visited list
	while next(visited) do
		local r = #visited
		local cell = visited[r]
		-- Chance to choose random cell instead of last visited
		if math.random() <= rand then
			r = math.random(#visited)
			cell = visited[r]
		end
		-- Find map tile at center of cell
		tx, ty = cellCenter(cell)
		-- Make list of adjacent empty (unvisited) cells
		local adjacent = {}
		-- TODO: figure out how to apply addLoop only to dead ends
		-- Currently, it has a chance of going east on every cell
		if cell.x < gridWidth and grid[cell.x+1][cell.y].vis == 0 then
			table.insert(adjacent, grid[cell.x+1][cell.y])
		end
		if cell.x > 1 and grid[cell.x-1][cell.y].vis == 0 then
			table.insert(adjacent, grid[cell.x-1][cell.y])
		end
		if cell.y < gridHeight and grid[cell.x][cell.y+1].vis == 0 then
			table.insert(adjacent, grid[cell.x][cell.y+1])
		end
		if cell.y > 1 and grid[cell.x][cell.y-1].vis == 0 then
			table.insert(adjacent, grid[cell.x][cell.y-1])
		end
		-- If adjacent list > 0
		if next(adjacent) then
			-- Select random cell from adjacent list
			local newCell = adjacent[math.random(#adjacent)]
			-- Move to selected cell, carve path
			local nx, ny = cellCenter(newCell)
			if nx == tx then
				self:createTunnelV(ty, ny, nx)
				if math.random(4) == 1 then
					self:createTunnelV(ty, ny, nx+1)
				end
			else
				self:createTunnelH(tx, nx, ny)
				if math.random(4) == 1 then
					self:createTunnelH(tx, nx, ny+1)
				end
			end
			-- Add selected cell to visited list
			newCell.vis = 1
			cell.vis = cell.vis + 1
			table.insert(visited, newCell)
		else -- adjacent list is empty
			-- Chance of connecting to adjacent cell anyway
			local roomChance = 0.5
			if cell.vis == 1 then
				roomChance = 0.9
				if math.random() <= loop then
					local dir = math.random(4)
					if dir == 1 and cell.x < gridWidth then 
						local nx, ny = cellCenter(grid[cell.x+1][cell.y])
						self:createTunnelH(tx, nx, ny)
						if math.random(4) == 1 then
							self:createTunnelH(tx, nx, ny+1)
						end
					elseif dir == 2 and cell.x > 1 then 
						local nx, ny = cellCenter(grid[cell.x-1][cell.y])
						self:createTunnelH(tx, nx, ny)
						if math.random(4) == 1 then
							self:createTunnelH(tx, nx, ny+1)
						end
					elseif dir == 3 and cell.y < gridHeight then 
						local nx, ny = cellCenter(grid[cell.x][cell.y+1])
						self:createTunnelV(ty, ny, nx)
						if math.random(4) == 1 then
							self:createTunnelV(ty, ny, nx+1)
						end
					elseif dir == 4 and cell.y > 1 then 
						local nx, ny = cellCenter(grid[cell.x][cell.y-1])
						self:createTunnelV(ty, ny, nx)
						if math.random(4) == 1 then
							self:createTunnelV(ty, ny, nx+1)
						end
					end
				end
			end
			-- Carve room at cell, 80% chance
			if math.random() <= roomChance then
				newRoom = Rect:new((cell.x-1)*cellWidth+1, (cell.y-1)*cellHeight+1, 
								   cellWidth-3, cellHeight-3)
				self:createRoom(newRoom)
				table.insert(self.roomList, newRoom)
			end
			-- Remove current cell from visited list
			table.remove(visited, r)
		end
		
	end
	self:placeActorsInRooms(self.roomList, actorTable, chance)
end

function LevelMap:generateCave(player)
	-- Cellular Automata cavern generator, CURRENTLY UNFINISHED
	-- Cave generator doesn't use room list, use at your own peril
	local cave = ROT.Map.Cellular:new(self.width, self.height)
	cave:randomize(0.50)
	for j = 1, self.height do
		local i = self.width/2 - 2
		cave:set(i, j, 1)
		cave:set(i+1, j, 1)
		cave:set(i+2, j, 1)
		cave:set(i+3, j, 1)
	end
	local function caveCall(x, y, val) 
		if val == 0 then
			self.tiles[x][y] = Tile:new('#', { 0.6, 0.6, 0.6 }, { 0, 0, 0 }, true, true)
		elseif val == 1 then 
			self.tiles[x][y] = Tile:new('.', { 0.4, 0.4, 0.4 }, { 0, 0, 0 }, false, false)
		end
	end
	for i = 1, 4 do
		cave:create(caveCall)
	end
end

function LevelMap:generateTuto(player, numRooms, minSize, maxSize, overlap, actorTable, chance) 
	-- TODO: add parameters for generation algorithm(s) and tile palettes
	numRooms = numRooms or con.MAX_ROOMS
	minSize = minSize or con.MIN_ROOM_SIZE
	maxSize = maxSize or con.MAX_ROOM_SIZE
	overlap = overlap or false
	for i=1, numRooms do 
		w = math.random(minSize, maxSize)
		h = math.random(minSize, maxSize)
		x = math.random(1, self.width - w - 1)
		y = math.random(1, self.height - h - 1)

		local newRoom = Rect:new(x, y, w, h)
		local valid = true
		-- Abort if room intersects a previous room (Parameterize this?)
		for r in pairs(self.roomList) do
			if newRoom:intersect(self.roomList[r]) then 
				valid = false
				break
			end
		end
		if valid or overlap then 
			self:createRoom(newRoom)
			local nx, ny = newRoom:center() 
			-- Place player in first room
			if #self.roomList == 0 then
				self:placeActor(player, nx, ny)
			else
				-- Carve tunnels between centerpoint of previous room and current room
				local px, py = self.roomList[#self.roomList]:center()
				
				if math.random(2) == 1 then
					self:createTunnelH(px, nx, py)
					self:createTunnelV(py, ny, nx)
				else
					self:createTunnelV(py, ny, px)
					self:createTunnelH(px, nx, ny)
				end
			end
			table.insert(self.roomList, newRoom)
			--numRooms = numRooms + 1
		end
	end
	self:placeActorsInRooms(roomList, actorTable, chance)
end



return LevelMap