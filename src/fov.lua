local FOV = {
	
	iCallback = function(fovMap, x, y) 
		if x > 0 and x <= levelMap.width and y > 0 and y <= levelMap.height then
			if levelMap.tiles[x][y].opaque == false then
				return true
			end
		end
		return false
	end,
	
	cCallback = function(x, y, r, v) 
		if x > 0 and x <= levelMap.width and y > 0 and y <= levelMap.height then
			-- tile is in FOV
			levelMap.tiles[x][y].explored = true
			levelMap.tiles[x][y].inFOV = true
		end
	end,
}

return FOV