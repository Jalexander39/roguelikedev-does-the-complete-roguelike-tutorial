local class = require "lib.middleclass"

local Tile = class("Tile")

function Tile:initialize(char, fg, bg, blocks, opaque)
	self.char = char or ' '
	self.fg = fg or { 0.4, 0.4, 0.4 }
	self.bg = bg or { 0, 0, 0 }
	-- Note to self: Never default a boolean to true in Lua
	self.blocks = blocks or false 	  -- Prevent actors from moving here?
	self.opaque = opaque or false 	  -- Blocks line of sight?
	self.explored = false  -- Visible outside FOV?
	self.inFOV = 0
end

function Tile:darken(amount) 
	for i in #self.fg do
		self.fg[i] = self.fg[i] * (amount or 0.5)
		self.bg[i] = false
	end
end

return Tile