local class = require 'lib.middleclass'

local Item = class("Item")

function Item:initialize(name, x, y, char, color) 
	self.name = name or '???'
	self.x = x or 1
	self.y = y or 1
	self.char = char or '='
	self.color = color or { 1, 0, 1 }
end 

return Item