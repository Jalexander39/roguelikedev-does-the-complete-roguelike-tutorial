local class = require 'lib.middleclass'

local con = require 'src.config'

local Message = class('Message')
function Message:initialize(string, color) 
	self.str = string or ''
	self.color = color or { 0.8, 0.8, 0.8 }
end

local MessageLog = class('MessageLog')

function MessageLog:initialize(width, height, maxLength) 
	self.width = width or con.CAM_WIDTH
	self.height = height or con.LOG_HEIGHT
	self.maxLength = self.height --maxLength or con.SCREEN_HEIGHT
	self.log = {}
end

function MessageLog:addString(string, color)
	if #self.log == self.maxLength then
		table.remove(self.log, 1)
	end
	table.insert(self.log, Message:new(string, color))
end

function MessageLog:printShort(disp) 
	if next(self.log) then
		local y = 1
		local index = 1
		local maxY = self.height
		if #self.log > maxY then
			index = #self.log - maxY + 1
		else
			maxY = #self.log
		end
		for i = index, maxY do
			--local str = self.log[i]
			-- TODO: Implement wordwrap
			disp:write(self.log[i].str, 2, y, self.log[i].color)
			y = y + 1
		end
	else print('Log is empty!') end
end

return MessageLog