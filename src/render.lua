local con = require 'src.config'
local FOV = require 'src.fov'

local render = {
	drawPlayfield = function(disp, player, levelMap, visObjects)
		-- Calculate camera position
		local cx = player.x - (con.CAM_WIDTH / 2)
		cx = math.max(0, math.min(cx, levelMap.width-con.CAM_WIDTH))
		local cy = player.y - (con.CAM_HEIGHT / 2)
		cy = math.max(0, math.min(cy, levelMap.height-con.CAM_HEIGHT))
		-- Draw terrain
		for y = 1, con.CAM_HEIGHT do
			for x = 1, con.CAM_WIDTH do
				local tile = levelMap.tiles[x+cx][y+cy]
				if tile.explored then
					-- Only draw tiles we've seen; darken if outside current FOV
					local fg = {tile.fg[1], tile.fg[2], tile.fg[3]}
					local bg = {tile.bg[1], tile.bg[2], tile.bg[3]}
					if not tile.inFOV then
						for i = 1, 3 do
							fg[i] = fg[i] * 0.5
							bg[i] = 0
						end
					end
					-- Offset Y to place playfield below message log
					disp:write(tile.char, x, y+con.LOG_HEIGHT, fg, bg)
				end
				-- x/y + cam offset
			end
		end
		-- Draw items and actors
		for i in pairs(visObjects) do
			local item = visObjects[i]
			if item.x > cx and item.x <= cx + con.CAM_WIDTH and 
			item.y > cy and item.y <= cy + con.CAM_HEIGHT then
				local bg = levelMap.tiles[item.x][item.y].bg
				disp:write(item.char, item.x-cx, item.y-cy+con.LOG_HEIGHT, item.color, bg)
			end
		end
		-- Draw actors
		-- local al = levelMap.actorList
		-- for a in pairs(al) do
		-- 	-- Check if actor is inside playfield (and FOV)
		-- 	if levelMap.tiles[al[a].x][al[a].y].inFOV and al[a].x > cx and al[a].y > cy and 
		-- 	al[a].x <= cx + con.CAM_WIDTH and al[a].y <= cy + con.CAM_HEIGHT then
		-- 		local bg = levelMap.tiles[al[a].x][al[a].y].bg
		-- 		disp:write(al[a].char, al[a].x-cx, al[a].y-cy+con.LOG_HEIGHT, al[a].color, bg)
		-- 	end
		-- end
	end,

	drawHUD = function(disp, player, levelMap, visObjects) 
		local x = con.CAM_WIDTH -- offset based on camera
		local hpPercent = player.hp / player.maxHP
		local fg1 = math.max(0, (hpPercent - 0.5) * 2)
		local fg2 = math.min(1, hpPercent * 2)
		disp:write('HP: ' .. player.hp .. '/' .. player.maxHP, x+2, 2, { 1, fg2, fg1 })
		local y = 5
		disp:write('You see here: ', x+2, y)
		for a in pairs(visObjects) do 
			local item = visObjects[a]
			if item ~= player then
				y = y + 1
				disp:write(item.char, x+2, y, item.color)
				disp:write(item.name, x+4, y)
				if item.hp then
					hpPercent = (item.hp / item.maxHP) * 100
					y = y+1
					disp:write('(HP: ' .. hpPercent .. '%)', x+5, y)
				end
			end
		end
	end,

	recomputeFOV = function(player, levelMap, fovMap) 
		for i = 1, levelMap.width do
			for j = 1, levelMap.height do
				levelMap.tiles[i][j].inFOV = false
			end
		end
		fovMap:compute(player.x, player.y, con.FOV_RADIUS, FOV.cCallback)
		local visObjects = {}
		for i in pairs(levelMap.itemList) do
			if levelMap.tiles[levelMap.itemList[i].x][levelMap.itemList[i].y].inFOV then
				table.insert(visObjects, levelMap.itemList[i])
			end
		end
		for a in pairs(levelMap.actorList) do
			if levelMap.tiles[levelMap.actorList[a].x][levelMap.actorList[a].y].inFOV then
				table.insert(visObjects, levelMap.actorList[a])
			end
		end
		return visObjects
	end,
}

return render