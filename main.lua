local class = require 'lib.middleclass'
local ROT = require 'lib.rot'

local Actor = require 'src.actor'
local con = require 'src.config'
local FOV = require 'src.fov'
local keyInput = require 'src.keyInput'
local LevelMap = require 'src.levelMap'
local MessageLog = require 'src.msgLog'
local render = require 'src.render'
local Tile = require 'src.tile'

function appendResults(r1, r2) 
	if r2.messages and next(r2.messages) then
		for m in pairs(r2.messages) do
			table.insert(r1.messages, r2.messages[m])
		end
	end
	if r2.deadActors and next(r2.deadActors) then
		for a in pairs(r2.deadActors) do
			table.insert(r1.deadActors, r2.deadActors[a])
		end
	end
end

-- TODO: Place HUD outside playfield without using ROT

-- Initialization
function love.load()
	-- TODO: Switch to ROT random number generator
	math.randomseed(os.time())
	print()
	-- Initialize level map
	player = Actor:new('player', 30, 20, '@', { 1, 1, 1 },
					 -- mHP, mSP, STR, INT, AGI, CON
						 30,  10,  10,  10,  10,  10)
	player.turn = false
	levelMap = LevelMap:new()
	local actorTable = {
		-- TODO: add weights to random actor selection
		{ name='gnoll', char='G', color={ 1, 0.8, 0.2 }, 
			hp=10, sp=10, str=12, int=10, agi=5, con=8 },
		{ name='bat', char='b', color={ 1, 0.2, 0 }, 
			hp=10, sp=10, str=8, int=10, agi=15, con=1 },
		{ name='lizard', char='l', color={ 0, 0.8, 0.5 }, 
			hp=10, sp=10, str=6, int=10, agi=10, con=5 },
	}
	-- generateMaze(player, cellSize X, Y, branchRandomness, loopPercentage)
	-- generateTuto(player, numRooms, roomMinSize, roomMinSize, overlap, actorTable, actorChance)
	levelMap:generateTuto(player, 30, 4, 8, true, actorTable, 0.5)
	levelMap:addActor(player)
	fovMap = ROT.FOV.Precise:new(FOV.iCallback)
	visObjects = render.recomputeFOV(player, levelMap, fovMap)
	fovRecompute = false
	turnResults = { messages = {}, deadActors = {} }
	msgLog = MessageLog:new(con.CAM_WIDTH, con.LOG_HEIGHT, con.SCREEN_HEIGHT)
	msgLog:addString("Do you have what it takes to survive the Labrynth of Peril?")
	-- Initialize scheduler
	for a in pairs(levelMap.actorList) do
		levelMap.scheduler:add(levelMap.actorList[a], true)
	end
	-- Initialize window, grid of 16x16 tiles
	disp = ROT.TextDisplay(con.SCREEN_WIDTH, con.SCREEN_HEIGHT, con.TILE_FONT, con.TILE_SIZE)
	disp:setDefaultForegroundColor({ 0.9, 0.9, 0.9 })
end

-- Window loses focus
function love.focus(f)
end

-- Input
function love.keypressed(key)
	local act = keyInput.keys[key]
	local action = keyInput.actions[act]
	if action then
		local results = action(player, levelMap)
		if results and next(results) then
			if results.moved then fovRecompute = true end
			appendResults(turnResults, results)
			player.turn = false
		end
	end
end

function love.mousepressed(x, y, button, istouch)
end

-- Update
function love.update(dt)
	if player.DEAD then return end
	while not player.turn do
		if next(turnResults.messages) then
			for m in pairs(turnResults.messages) do
				-- messageLog.add(results.messages[m])
				msgLog:addString(turnResults.messages[m][1], turnResults.messages[m][2])
				--print(turnResults.messages[m])
			end
			turnResults.messages = {}
		end
		if next(turnResults.deadActors) then
			for a in pairs(turnResults.deadActors) do
				if turnResults.deadActors[a] == player then
					player.char = '%'
					player.color = { 0.8, 0, 0 }
					player.DEAD = true
					disp:clear()
					render.drawPlayfield(disp, player, levelMap, visObjects)
					render.drawHUD(disp, player, levelMap, visObjects)
					msgLog:addString('Alas, your adventure is at an end...', { 1, 0.3, 0 } )
					msgLog:printShort(disp)
					return
				end
				levelMap:killActor(turnResults.deadActors[a])
			end
			turnResults.deadActors = {}
		end
		local currentActor = levelMap.scheduler:next()
		if currentActor == player then
			-- Redraw playfield
			disp:clear()
			if fovRecompute then
				visObjects = render.recomputeFOV(player, levelMap, fovMap)
				--fovRecompute = false
			end
			render.drawPlayfield(disp, player, levelMap, visObjects)
			-- Draw HUD, print message log
			render.drawHUD(disp, player, levelMap, visObjects)
			msgLog:printShort(disp)
			-- Break until end of player turn
			player.turn = true
			-- print('DT: ' .. dt)
			-- Player turn is handled in keypressed() event
			return
		end	-- Take monster turn
		-- Perform action based on AI
		local results = currentActor.ai:move(player, levelMap)
		if results and next(results) then
			appendResults(turnResults, results)
		end
		-- Update game state, pass messages to log
	end
end

-- Render
function love.draw()
	-- Blit to screen
	disp:draw()
end

-- Exit
function love.quit()
end